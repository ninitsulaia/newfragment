package com.example.newfragment.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.newfragment.R

class FragmentFirst: Fragment(R.layout.first_fragment) {

    private lateinit var editTextNote: EditText
    private lateinit var buttonAdd: Button
    private lateinit var textView: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editTextNote = view.findViewById(R.id.editTextNote)
        val sharedPreferences = requireActivity().getSharedPreferences("nini", Context.MODE_PRIVATE)
        val text = sharedPreferences.getString("NINI", "")
        textView.text = text
        init()
        buttonAdd.setOnClickListener {
            var note = editTextNote.text.toString()
            var text1 = textView.text.toString()
            var result = note + "\n" + text1
            textView.text = result
            sharedPreferences.edit().putString("NINI", result).apply()
        }
    }
    private fun init() {
        editTextNote = requireView().findViewById(R.id.editTextNote)
        buttonAdd = requireView().findViewById(R.id.buttonAdd)
        textView = requireView().findViewById(R.id.textView)
    }
}