package com.example.newfragment.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.newfragment.fragment.FragmentFirst
import com.example.newfragment.fragment.FragmentSecond
import com.example.newfragment.fragment.FragmentThird

class ViewPagerFragmentAdapter(activity: FragmentActivity):FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> FragmentFirst()
            1 -> FragmentSecond()
            2 -> FragmentThird()
            else -> FragmentThird()
        }
    }
}